package br.com.fernandesworkgroup.andronate;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;

import br.com.fernandesworkgroup.andronate.dao.DoadorDAO;
import br.com.fernandesworkgroup.andronate.helper.FormularioHelper;
import br.com.fernandesworkgroup.andronate.model.Doador;

/**
 * Created by Anderson Fernandes on 21/06/2016.
 */
public class CadastroActivity extends AppCompatActivity {

    public static final int CODIGO_CAMERA = 567;
    private FormularioHelper helper;
    private String caminhoFoto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        Spinner condicao = (Spinner) findViewById(R.id.cadastro_condicao);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.condicao_array, android.R.layout.simple_spinner_dropdown_item);
        condicao.setAdapter(adapter);

        helper = new FormularioHelper(this);

        ImageButton btnFoto = (ImageButton) findViewById(R.id.cadastro_foto);

        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                caminhoFoto = getExternalFilesDir(null) + "/"+ System.currentTimeMillis() +".jpg";
                File arquivoFoto = new File(caminhoFoto);
                intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(arquivoFoto));
                startActivityForResult(intentCamera, CODIGO_CAMERA);
            }
        });

        Button btnCadastro = (Button) findViewById(R.id.cadastro_botao_cadastrar);

        btnCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrar();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            switch (resultCode){
                case CODIGO_CAMERA:
                    Toast.makeText(CadastroActivity.this,"Foto enviada com Sucesso",Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflate = getMenuInflater();
        inflate.inflate(R.menu.menu_cadastro, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()){
          case R.id.menu_doar:
              cadastrar();
              break;
      }

        return super.onOptionsItemSelected(item);
    }

    private void cadastrar(){

        Doador doador = helper.getAluno();

        if(helper.hasCondicaoDeCadastro()){
            DoadorDAO dao = new DoadorDAO(this);
            dao.insere(doador);
            dao.close();
            Toast.makeText(CadastroActivity.this,"Doação feita com sucesso",Toast.LENGTH_SHORT).show();
            finish();
        }else{
            Toast.makeText(CadastroActivity.this,"Insira o modelo do Smarthphone",Toast.LENGTH_SHORT).show();
        }

    }
}
