package br.com.fernandesworkgroup.andronate;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import br.com.fernandesworkgroup.andronate.adapter.DoadorAdapter;
import br.com.fernandesworkgroup.andronate.dao.DoadorDAO;
import br.com.fernandesworkgroup.andronate.model.Doador;


/**
 * Created by Anderson Fernandes on 20/06/2016.
 */
public class FeedActivity extends AppCompatActivity {

    private ListView listaDoador ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        listaDoador = (ListView) findViewById(R.id.lista_doador);


    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();
    }

    private void carregaLista(){
        DoadorDAO dao = new DoadorDAO(this);
        List<Doador> doador = dao.listaDoador();
        dao.close();

        if(doador.size() > 0){
            DoadorAdapter adapter = new DoadorAdapter(FeedActivity.this,doador);

            listaDoador.setAdapter(adapter);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cadastro:
                Intent intent = new Intent(FeedActivity.this, CadastroActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
