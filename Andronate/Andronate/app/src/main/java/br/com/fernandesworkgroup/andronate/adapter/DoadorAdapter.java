package br.com.fernandesworkgroup.andronate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.fernandesworkgroup.andronate.R;
import br.com.fernandesworkgroup.andronate.model.Doador;

/**
 * Created by PC-CASA on 21/06/2016.
 */
public class DoadorAdapter extends BaseAdapter {

    private final Context context;
    private final List<Doador> doadores;

    public DoadorAdapter(Context context,List<Doador> doadores){
        this.context = context;
        this.doadores = doadores;
    }

    @Override
    public int getCount() {
        return doadores.size();
    }

    @Override
    public Object getItem(int position) {
        return doadores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return doadores.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Doador doador = doadores.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = convertView;

        if(view == null){
            view = inflater.inflate(R.layout.list_item,parent,false);
        }

        TextView campoSmarthphone = (TextView) view.findViewById(R.id.item_smathphone);
        campoSmarthphone.setText(doador.getNome());

        TextView campoDoador = (TextView) view.findViewById(R.id.item_doador);
        campoDoador.setText(doador.getDoador());

        return view;
    }
}
