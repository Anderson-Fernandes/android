package br.com.fernandesworkgroup.andronate.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.fernandesworkgroup.andronate.model.Doador;

/**
 * Created by Anderson Fernandes on 21/06/2016.
 */
public class DoadorDAO extends SQLiteOpenHelper {

    public DoadorDAO(Context context){
        super(context,"andronate",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE doador(id INTEGER PRIMARY KEY, nome TEXT NOT NULL, data TEXT, condicao TEXT NOT NULL, descricao TEXT, doador TEXT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         String sql = "";
        switch (oldVersion){
            case 1 : sql = "";
                db.execSQL(sql);
        }
    }

    public void insere(Doador doador){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = getDadosDoDoador(doador);

        db.insert("doador",null,dados);
    }

    @NonNull
    private ContentValues getDadosDoDoador(Doador doador){
        ContentValues dados = new ContentValues();
        dados.put("nome", doador.getNome());
        dados.put("data",doador.getData());
        dados.put("condicao",doador.getCondicao());
        dados.put("descricao",doador.getDesc());
        dados.put("doador",doador.getDoador());

        return dados;
    }

    public List<Doador> listaDoador(){
        String sql = "SELECT * FROM doador ORDER BY id DESC;";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        List<Doador> doadores = new ArrayList<>();

        while (c.moveToNext()){
            Doador doador = new Doador();

            doador.setId(c.getLong(c.getColumnIndex("id")));
            doador.setNome(c.getString(c.getColumnIndex("nome")));
            doador.setData(c.getString(c.getColumnIndex("data")));
            doador.setCondicao(c.getString(c.getColumnIndex("caminhoFoto")));
            doador.setDesc(c.getString(c.getColumnIndex("descricao")));
            doador.setDoador(c.getString(c.getColumnIndex("doador")));

            doadores.add(doador);
        }

        c.close();

        return doadores;

    }

}
