package br.com.fernandesworkgroup.andronate.helper;

import android.app.Activity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import br.com.fernandesworkgroup.andronate.R;
import br.com.fernandesworkgroup.andronate.model.Doador;

/**
 * Created by FernandesWorkgroup on 19/04/2016.
 */
public class FormularioHelper {
   private Doador doador;
   private final EditText campoModelo;
   private final EditText campoCompra;
   private final Spinner campoCondicao;
   private final ImageView campoFoto;
   private final EditText campoDesc;
   private final EditText campoDoador;


    public FormularioHelper(Activity activity) {
        this.doador = new Doador();
        this.campoModelo = (EditText) activity.findViewById(R.id.cadastro_modelo);
        this.campoCompra = (EditText) activity.findViewById(R.id.cadastro_compra);
        this.campoCondicao = (Spinner) activity.findViewById(R.id.cadastro_condicao);
        this.campoFoto = (ImageView) activity.findViewById(R.id.cadastro_foto);
        this.campoDesc = (EditText) activity.findViewById(R.id.cadastro_desc);
        this.campoDoador = (EditText) activity.findViewById(R.id.cadastro_doador);
    }

    public Doador getAluno(){
        doador.setNome(campoModelo.getText().toString());
        doador.setData(campoCompra.getText().toString());
        doador.setCondicao(campoCondicao.getSelectedItem().toString());
        doador.setCaminhoFoto((String) campoFoto.getTag());
        doador.setDesc(campoDesc.getText().toString());
        doador.setDoador(campoDoador.getText().toString());

        return this.doador;
    }

    public boolean hasCondicaoDeCadastro(){
        if(campoModelo.getText().toString().length() > 0){
            return true;
        }else{
            return false;
        }
    }


}
