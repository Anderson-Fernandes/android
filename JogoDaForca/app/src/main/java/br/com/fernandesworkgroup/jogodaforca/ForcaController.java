package br.com.fernandesworkgroup.jogodaforca;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by PC-CASA on 22/04/2016.
 */
public class ForcaController {

    private String palavraParaAdivinhar; //Palavra a ser advinhada
    private Set<Character> letrasUsadas; //Lista que conterá as letras que foram utilizadas sem itens repetidos
    private int quantidadeDeErros = -1; //Quantidades de erros;

    public ForcaController(String palavra) {
        this.palavraParaAdivinhar = palavra;
        this.letrasUsadas = new HashSet<>();
    }

    public int getQuantidadeDeErros() {
        return quantidadeDeErros;
    }

    public void joga(Character letra){
        // Caso Set contenha a letra jogada, saimos da  função
        if(letrasUsadas.contains(letra))return;
        letrasUsadas.add(letra);

        if(palavraParaAdivinhar.contains(letra.toString()))return;
        quantidadeDeErros++;
    }

    public String getPalavraAteAgora(){
        String visualizacao = "";
        for(char c: palavraParaAdivinhar.toCharArray()){
            if(letrasUsadas.contains(c)){
                visualizacao += c;
            }else{
                visualizacao += " ";
            }
        }
        return visualizacao;
    }

    public boolean isMorreu(){
        return getQuantidadeDeErros() == 5;
    }

    public boolean isGanhou(){
        return !getPalavraAteAgora().contains(" ");
    }

    public boolean isTerminou(){
        return  isMorreu() || isGanhou();
    }
}
