package br.com.fernandesworkgroup.jogodaforca;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.Toast;

/**
 * Created by PC-CASA on 22/04/2016.
 */
public class ForcaView extends PlanoCartesianoView {

    private enum Membro{braco, perna}
    private enum Lado{direto, esquerdo}
    //Responsavel por armazenar
    //Todas as figuras geometricas
    // A serem desenhadas

    private Path pathForca;
    private Paint paintForca;
    private ForcaController forcaController;
    private boolean vaiResetar;


    public void setVaiResetar(boolean vaiResetar) {
        this.vaiResetar = vaiResetar;
    }

    public boolean isVaiResetar() {
        return vaiResetar;
    }

    public ForcaView(Context context) {
        super(context);
    }

    public ForcaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPathForca(new Path());
    }

    public Path getPathForca() {
        return pathForca;
    }

    public void setPathForca(Path pathForca) {
        this.pathForca = pathForca;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        setDesenhaPlanoCartesiano(false); //Aplique como True para -> Aplicar debug de tela
        plotaArmacaoDaForca();

        drawLetrasCorretas(canvas);

            if(getForcaController() != null) {
                switch (getForcaController().getQuantidadeDeErros()) {
                    case 5:

                        plotaMembro(Membro.perna, Lado.esquerdo);
                        reDraw(canvas);
                        break;
                    case 4:
                        plotaMembro(Membro.perna, Lado.direto);
                        break;
                    case 3:
                        plotaMembro(Membro.braco, Lado.esquerdo);
                        break;
                    case 2:
                        plotaMembro(Membro.braco, Lado.direto);
                        break;
                    case 1:
                        plotaCorpo();
                        break;
                    case 0:
                        plotaCabeca();
                        break;
                }

            }


        plotaTracos();

        if(isVaiResetar()){
            reDraw(canvas);
            setVaiResetar(false);
        }

        canvas.drawPath(getPathForca(), getPaintForca());
    }



    private void reDraw(Canvas canvas) {
        if(getForcaController() != null){
            if(getForcaController().isTerminou()){
                setPathForca(new Path());
                plotaArmacaoDaForca();
                canvas.drawText("",0,0,0,0,new Paint());
                drawLetrasCorretas(canvas);
            }
        }

    }

    public Paint getPaintForca() {
        paintForca = new Paint();
        paintForca.setColor(Color.BLACK);
        paintForca.setStyle(Paint.Style.STROKE);
        paintForca.setStrokeWidth(8);
        return paintForca;
    }

    private void plotaArmacaoDaForca(){
        getPathForca().moveTo(toPixel(1),toPixel(10));
        getPathForca().lineTo(toPixel(3), toPixel(10));

        getPathForca().moveTo(toPixel(2),toPixel(10));
        getPathForca().lineTo(toPixel(2),toPixel(1));

        getPathForca().rLineTo(toPixel(5),0);

        getPathForca().rLineTo(0,toPixel(1));
    }

    private void plotaCabeca(){
        getPathForca().addCircle(toPixel(7),toPixel(3),toPixel(1),Path.Direction.CW);
    }

    private void plotaCorpo(){
        getPathForca().moveTo(toPixel(7), toPixel(4));
        getPathForca().lineTo(toPixel(7), toPixel(7));
    }

    private void plotaMembro(Membro membro, Lado lado){
        final int posicaoDoCorpo = 7; //posição do corpo no eixo X
        final int alturaDoBraco = 5; //Def. o Y onde será desenhado o braço do boneco
        final int alturaDaPerna = 7; // Def. o Y onde será desenhado a perna do boneco
        int alturaFinal;

        if(membro == Membro.braco){
            getPathForca().moveTo(toPixel(posicaoDoCorpo), toPixel(alturaDoBraco));
            alturaFinal = alturaDoBraco+1 ;
        }else{
            getPathForca().moveTo(toPixel(posicaoDoCorpo), toPixel(alturaDaPerna));
            alturaFinal = alturaDaPerna+1;
        }

        if(lado == Lado.direto){
            getPathForca().lineTo(toPixel(posicaoDoCorpo+1), toPixel(alturaFinal));
        }else{
            getPathForca().lineTo(toPixel(posicaoDoCorpo-1),toPixel(alturaFinal));
        }
    }

    private void plotaTracos(){
        int eixoX = toPixel(3);
        getPathForca().moveTo(eixoX+10, toPixel(10));

        if(getForcaController()==null)return;

        for(int i = 0; i <= getForcaController().getPalavraAteAgora().length()-1;i++){
            getPathForca().rMoveTo(10, 0);
            getPathForca().rLineTo(toPixel(1), 0);
        }
    }

    public ForcaController getForcaController() {
        return forcaController;
    }

    public void setForcaController(ForcaController forcaController) {
        this.forcaController = forcaController;
    }

    private Paint getPaintTraco(){
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(2);
        paint.setTextSize(25);

        return paint;
    }

    private void drawLetrasCorretas (Canvas canvas){
        int eixoX = toPixel(3);
        getPathForca().moveTo(eixoX + 10, toPixel(10));
        eixoX += 35;

        if(getForcaController()== null)return;

        for(int i=0; i<=getForcaController().getPalavraAteAgora().length()-1; i++){
            char c = getForcaController().getPalavraAteAgora().charAt(i);


            canvas.drawText(c+"",eixoX+((toPixel(1)+10) * i), toPixel(10)-15, getPaintTraco());
        }

    }
}
