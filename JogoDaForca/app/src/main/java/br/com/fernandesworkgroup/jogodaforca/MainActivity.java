package br.com.fernandesworkgroup.jogodaforca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button btJogar;
    private Button btPlay;
    private EditText etLetra;
    private ForcaView forcaview;
    private ForcaController forcaController;
    private String[] palavras = new String[]{"maça", "banana", "pera", "goiaba"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btJogar = (Button) findViewById(R.id.btJogar);
        btPlay = (Button) findViewById(R.id.btPlay);
        etLetra = (EditText) findViewById(R.id.etLetra);

        forcaview = (ForcaView) findViewById(R.id.fvJogo);

        init();
    }

    private void init(){
        btJogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etLetra.getText().toString().trim().length() ==0)return;

                getForcaController().joga(etLetra.getText().toString().trim().charAt(0));

                forcaview.invalidate();
                etLetra.getText().clear();

                if(getForcaController().isTerminou()){
                    btJogar.setEnabled(false);
                    etLetra.setEnabled(false);
                    btPlay.setEnabled(true);

                    Toast.makeText(getApplicationContext(), (getForcaController().isMorreu())?"Ops..Você perdeu":"Parabéns, você ganhou",Toast.LENGTH_LONG).show();
                }
            }
        });


        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setForcaController(new ForcaController(palavras[new Random().nextInt(palavras.length)]));
                forcaview.setForcaController(getForcaController());
                forcaview.setVaiResetar(true);
                forcaview.invalidate();

                etLetra.getText().clear();
                etLetra.setEnabled(true);
                btJogar.setEnabled(true);
                btPlay.setEnabled(false);
            }
        });
    }

    public ForcaController getForcaController() {
        return forcaController;
    }

    public void setForcaController(ForcaController forcaController) {
        this.forcaController = forcaController;
    }
}
