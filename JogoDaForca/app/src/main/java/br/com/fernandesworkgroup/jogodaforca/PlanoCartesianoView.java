package br.com.fernandesworkgroup.jogodaforca;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by PC-CASA on 22/04/2016.
 */
public class PlanoCartesianoView extends View {

    // Armazena o menor lado do nosso display
    // caso o display for maior na vertical
    // a variavel guardara esse valor
    // do contrario será guardado o valor da dimensão da nossa tela
    // na horizontal

    private int menorLadoDisplay;
    private int unidade;
    private boolean desenhaPlanoCartesiano;

    public PlanoCartesianoView(Context context) {
        super(context);
        this.desenhaPlanoCartesiano = false;
    }

    public PlanoCartesianoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.desenhaPlanoCartesiano = false;
    }

    public void drawPlanoCartesiano(Canvas canvas){
        Path path = new Path();

        int max = toPixel(10);
        for(int n = 0; n <= 10; n++){
            // desenhando as linhas verticais
            path.moveTo(toPixel(n),1);
            path.lineTo(toPixel(n), max);
            // desenhando as linhas na horizontal
            path.moveTo(1,toPixel(n));
            path.lineTo(max,toPixel(n));
        }

        Paint paint = new Paint();

        paint.setAntiAlias(true); //Propriedade que define a suavidade da linha
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);

        canvas.drawPath(path, paint);
    }

    protected int toPixel(int vezes) {
        return vezes * getUnidade();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        setMenorLadoDisplay( (getHeight() > getWidth())? getWidth(): getHeight() ) ;

        setUnidade((getMenorLadoDisplay()/10));

        if(isDesenhaPlanoCartesiano())
        drawPlanoCartesiano(canvas);
    }

    public int getUnidade() {
        return unidade;
    }

    public void setUnidade(int unidade) {
        this.unidade = unidade;
    }

    public int getMenorLadoDisplay() {
        return menorLadoDisplay;
    }

    public void setMenorLadoDisplay(int menorLadoDisplay) {
        this.menorLadoDisplay = menorLadoDisplay;
    }

    public boolean isDesenhaPlanoCartesiano() {
        return desenhaPlanoCartesiano;
    }

    public void setDesenhaPlanoCartesiano(boolean desenhaPlanoCartesiano) {
        this.desenhaPlanoCartesiano = desenhaPlanoCartesiano;
    }
}
