package br.com.fernandesworkgroup.jogos2d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by PC-CASA on 22/04/2016.
 */
public class EstudoView extends View{

    public EstudoView(Context context) {
        super(context);
    }

    public EstudoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void desenhaLinha(Canvas canvas){

        Paint paint = new Paint();
        paint.setStrokeWidth(0);
        paint.setColor(Color.BLUE);

        for(int n = 1; n <= 20; n++){
            paint.setStrokeWidth(n);
            canvas.drawLine(10,(n*20),100,(n*20), paint);
        }

    }

    private void desenhaRetangulo(Canvas canvas){
        canvas.drawRect(new Rect(200,200,300,300),new Paint());

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        canvas.drawLine(200,200,300,300, paint);

        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(10);

        paint.setStyle(Paint.Style.STROKE);
        for(int i = 0; i <= 20; i++){
            canvas.drawRect(new Rect(300 + (i*20*-1),300+(i*20*-1),400+(i*20),400+(i*20)), paint);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //desenhaLinha(canvas);
        //desenhaRetangulo(canvas);

        Paint paint = new Paint();
        Paint paintCircule = new Paint();
        paintCircule.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(100,100,100,paintCircule);
        paint.setColor(Color.YELLOW);
        canvas.drawLine(0,100,100,100,paint);
        canvas.drawLine(100,100,100,0,paint);
    }
}
