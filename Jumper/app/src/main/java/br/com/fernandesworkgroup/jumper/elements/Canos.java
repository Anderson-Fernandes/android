package br.com.fernandesworkgroup.jumper.elements;

import android.content.Context;
import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import br.com.fernandesworkgroup.jumper.engine.Som;
import br.com.fernandesworkgroup.jumper.graphic.Tela;

/**
 * Created by PC-CASA on 24/04/2016.
 */
public class Canos {

    private static final int DISTANCIA_ENTRE_CANOS = 200;
    private static final int QUANTIDADE_DE_CANOS = 5;
    private final List<Cano> canos = new ArrayList<>();
    private Tela tela;
    private Pontuacao pontuacao;
    private Context context;
    private Som som;

    public Canos(Tela tela, Pontuacao pontuacao, Context context, Som som) {
        this.tela = tela;
        this.pontuacao = pontuacao;
        this.context = context;
        this.som = som;
        int posicao = 400;

        for (int i = 0; i < QUANTIDADE_DE_CANOS ;i++){
            posicao += DISTANCIA_ENTRE_CANOS;
            Cano cano = new Cano(tela,posicao, context);
            canos.add(cano);
        }
    }

    public void desenhaNo(Canvas canvas){
        for (Cano cano:canos) {
            cano.desenhaNo(canvas);
        }
    }

    public void move(){
        ListIterator<Cano> interator = canos.listIterator();
        while (interator.hasNext()){
            Cano cano = interator.next();
            cano.move();
            if(cano.saiuDaTela()){
                pontuacao.aumenta();
                interator.remove();
                //criar outro cano
                Cano outroCano = new Cano(tela, getMaxima() + DISTANCIA_ENTRE_CANOS, context);
                interator.add(outroCano);
            }
        }
    }

    public int getMaxima() {
        int maximo = 0;
        for(Cano cano: canos){
            maximo = Math.max(cano.getPosicao(), maximo);
        }
        return maximo;
    }

    public boolean temColisaoCom(Passaro passaro) {
        for(Cano cano: canos){
            if(cano.temColisaoHorizontalCom(passaro) && cano.temColisaoVerticalCom(passaro)){
                som.toca(Som.COLISAO);
                return true;
            }
        }
        return  false;
    }
}
