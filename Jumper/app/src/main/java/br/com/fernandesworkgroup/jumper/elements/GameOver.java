package br.com.fernandesworkgroup.jumper.elements;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import br.com.fernandesworkgroup.jumper.graphic.Cores;
import br.com.fernandesworkgroup.jumper.graphic.Tela;

/**
 * Created by PC-CASA on 24/04/2016.
 */
public class GameOver {

    private static final String GAME_OVER =  "Game Over";
    private static final Paint VERMELHO = Cores.getCorDoGameOver();
    private Tela tela;

    public GameOver(Tela tela) {
        this.tela = tela;
    }

    public void desenhaNo(Canvas canvas){
        int centralizaHorizontal = centralizaText(GAME_OVER);
        canvas.drawText(GAME_OVER,centralizaHorizontal,(tela.getAltura()/2),VERMELHO);
    }

    private int centralizaText(String texto){
        Rect limiteDoTexto = new Rect();
        VERMELHO.getTextBounds(texto,0,texto.length(), limiteDoTexto);
        int centroHorizontal = tela.getLargura()/2 - (limiteDoTexto.right - limiteDoTexto.left)/2;
        return centroHorizontal;
    }
}
