package br.com.fernandesworkgroup.jumper.elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import br.com.fernandesworkgroup.jumper.R;
import br.com.fernandesworkgroup.jumper.engine.Som;
import br.com.fernandesworkgroup.jumper.graphic.Cores;
import br.com.fernandesworkgroup.jumper.graphic.Tela;

/**
 * Created by PC-CASA on 24/04/2016.
 */
public class Passaro {

    public static final float X = 100;
    public static final int RAIO = 50;
    private static final Paint COR_VERMELHA = Cores.getCorDoPassaro();
    private final Bitmap passaro;
    private float altura;
    private Tela tela;
    private Som som;

    public Passaro(Tela tela, Context context, Som som) {
        this.tela = tela;
        this.som = som;
        this.altura = 100;
        Bitmap bp = BitmapFactory.decodeResource(context.getResources(), R.drawable.passaro);
        this.passaro = bp.createScaledBitmap(bp,RAIO*2,RAIO*2,false);
    }

    public void desenhaNo(Canvas canvas){
        //canvas.drawCircle(X, altura,RAIO,COR_VERMELHA);
        canvas.drawBitmap(passaro,X - RAIO,altura - RAIO,null);
    }

    public void cai() {
        boolean chegouNoChao = altura + RAIO > tela.getAltura();
        if(!chegouNoChao) {
            this.altura += 5;
        }
    }

    public void pula() {
        boolean chegouNoTeto = (altura - RAIO) > 0;
        if(chegouNoTeto){
            som.toca(Som.PULO);
            this.altura -=150;
        }
    }

    public float getAltura() {
        return altura;
    }
}
