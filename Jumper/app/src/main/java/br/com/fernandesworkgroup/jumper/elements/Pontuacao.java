package br.com.fernandesworkgroup.jumper.elements;

import android.graphics.Canvas;
import android.graphics.Paint;

import br.com.fernandesworkgroup.jumper.engine.Som;
import br.com.fernandesworkgroup.jumper.graphic.Cores;

/**
 * Created by PC-CASA on 24/04/2016.
 */
public class Pontuacao {
    private static final Paint BRANCO = Cores.getCorDaPontuacao();
    private final Som som;
    private int pontos = 0;

    public Pontuacao(Som som) {
        this.som = som;
    }

    public void desenhaNo(Canvas canvas) {
        canvas.drawText(String.valueOf(pontos),100,100,BRANCO);
    }


    public void aumenta() {
        som.toca(Som.PONTUACAO);
        pontos++;
    }
}
