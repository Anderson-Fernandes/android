package br.com.fernandesworkgroup.jumper.engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import br.com.fernandesworkgroup.jumper.R;
import br.com.fernandesworkgroup.jumper.elements.Canos;
import br.com.fernandesworkgroup.jumper.elements.GameOver;
import br.com.fernandesworkgroup.jumper.elements.Passaro;
import br.com.fernandesworkgroup.jumper.elements.Pontuacao;
import br.com.fernandesworkgroup.jumper.graphic.Tela;

/**
 * Created by PC-CASA on 24/04/2016.
 */
public class Game extends SurfaceView implements Runnable, View.OnTouchListener{
    private final Som som;
    private Tela tela;
    private boolean isRunning = true;
    private SurfaceHolder holder = getHolder();
    private Passaro passaro;
    private Bitmap background;
    private Canos canos;
    private Pontuacao pontuacao;
    private Context context;

    public Game(Context context) {
        super(context);
        this.context = context;
        tela = new Tela(context);
        som = new Som(context);

        inicializaElementos();

        setOnTouchListener(this);
    }

    private void inicializaElementos() {

        passaro = new Passaro(tela, context, som);
        pontuacao = new Pontuacao(som);
        canos = new Canos(tela,pontuacao, context,som);
        Bitmap back = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        background = Bitmap.createScaledBitmap(back, back.getWidth(),tela.getAltura(),false);

    }

    @Override
    public void run() {
        while(isRunning){
            if(!holder.getSurface().isValid())continue;
            Canvas canvas = holder.lockCanvas();
            // desenhar componentes do jogo;
            canvas.drawBitmap(background,0,0,null);
            passaro.desenhaNo(canvas);
            passaro.cai();

            canos.desenhaNo(canvas);
            canos.move();
            pontuacao.desenhaNo(canvas);

            if(new VerificadorDeColisao(passaro,canos).temColisao()){
                new GameOver(tela).desenhaNo(canvas);
                isRunning = false;
            }

            holder.unlockCanvasAndPost(canvas);
        }
    }

    public void inicia() {
        isRunning = true;
    }

    public void pausa() {
        isRunning = false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        passaro.pula();
        return false;
    }
}
