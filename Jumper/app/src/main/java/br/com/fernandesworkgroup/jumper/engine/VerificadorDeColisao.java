package br.com.fernandesworkgroup.jumper.engine;

import br.com.fernandesworkgroup.jumper.elements.Canos;
import br.com.fernandesworkgroup.jumper.elements.Passaro;

/**
 * Created by PC-CASA on 24/04/2016.
 */
public class VerificadorDeColisao {

    private Passaro passaro;
    private Canos canos;

    public VerificadorDeColisao(Passaro passaro, Canos canos) {
        this.passaro = passaro;
        this.canos = canos;
    }

    public boolean temColisao() {
        return canos.temColisaoCom(passaro);
    }
}
