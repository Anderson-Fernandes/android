package br.com.fernandesworkgroup.login;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BoasVindasActivity extends Activity{
	private SharedPreferences prefs;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_boas_vindas);
		
		prefs = getSharedPreferences(MainActivity.APP_PREFS, MODE_PRIVATE);
		
		final TextView name = (TextView) findViewById(R.id.nomeBoas);
		
		name.setText("Bem Vindo " + prefs.getString(MainActivity.USERNAME_KEY, "Erro"));
	}

}
