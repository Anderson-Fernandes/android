package br.com.fernandesworkgroup.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.Editable;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	final static String APP_PREFS = "app_prefs";
	final static String USERNAME_KEY = "username";
	final static String PASSWORD_KEY = "password";
	String username;
	String pass;
	SharedPreferences prefs;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        
        
        Button botaoEntrar =(Button) findViewById(R.id.botao_entrar);
        botaoEntrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, BoasVindasActivity.class);
				startActivity(intent);
			}
		});
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	Button btnEntrar =(Button) findViewById(R.id.botao_entrar);
    	Button btnCadastrar = (Button) findViewById(R.id.botao_cadastrar);
    	
    	prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
		username = prefs.getString(USERNAME_KEY, null);
		pass = prefs.getString(PASSWORD_KEY, null);
    	
		
		btnEntrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				EditText nome =(EditText) findViewById(R.id.nome);
				EditText senha =(EditText) findViewById(R.id.senha);
				String a = username.toString();
				
				if (username == null && pass == null) {
					Toast.makeText(MainActivity.this, "Ainda n�o h� cadastro", Toast.LENGTH_SHORT).show();
				}else{
					if(username.toString() == nome.getEditableText().toString() && pass.toString() == senha.getEditableText().toString()){
						Intent intent = new Intent(MainActivity.this,BoasVindasActivity.class);
						startActivity(intent);
					}else{
						Toast.makeText(MainActivity.this, "Nome de usuario ou senha incorreta", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		
    	btnCadastrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				EditText nome =(EditText) findViewById(R.id.nome);
				EditText senha =(EditText) findViewById(R.id.senha);
				Toast.makeText(MainActivity.this, nome.getEditableText().toString(), Toast.LENGTH_SHORT).show();
				
				if (nome.length() < 1 && senha.length() <1) {
					Toast.makeText(MainActivity.this, "Preencha todos os campos", Toast.LENGTH_SHORT).show();
				}else{
					String username = nome.getEditableText().toString();
					String password = senha.getEditableText().toString();
					Editor editor = prefs.edit();
					editor.putString(USERNAME_KEY, username);
					editor.putString(PASSWORD_KEY, password);
					editor.commit();
					
					
					Intent intent = new Intent(MainActivity.this , BoasVindasActivity.class);
					startActivity(intent);
				
				}
				
			}
		});

    }
    
   
}
