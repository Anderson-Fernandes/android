package com.example.radio;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText txtNome = (EditText)findViewById(R.id.txtNome);
        final RadioGroup grpOne = (RadioGroup)findViewById(R.id.grpOne);
        
        grpOne.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				boolean sim = R.id.rdbSim == checkedId;
				boolean nao = R.id.rdbNao == checkedId;
				if(sim){
					Log.i("CATEGORIA", "Marcou Sim" + checkedId);
				}else if(nao){
					Log.i("CATEGORIA", "Marcou N�o" + checkedId);
				}
			}
		});
        
        final CheckBox ckb = (CheckBox)findViewById(R.id.chkReceberEmail);
        ckb.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
			
        	
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				Log.i("CATEGORIA","check " + isChecked);
				
			}
		});
        
        Button btnEnviar = (Button)findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i("CATEGORIA", "OK");
				
				// Compara o id do radioSIm
				boolean concorda = R.id.rdbSim == grpOne.getCheckedRadioButtonId();
				boolean receberEmail =  ckb.isChecked();
				
				StringBuffer sb = new StringBuffer();
				sb.append("Nome: ").append(txtNome.getText()).append("\nReceber Email: " ).append(receberEmail?"Sim":"N�o").append("\nConcorda: ").append(concorda ? "Sim" : "N�o");
				Toast.makeText(getApplicationContext(), sb.toString(), Toast.LENGTH_LONG).show();
			}
		});
    } 

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
