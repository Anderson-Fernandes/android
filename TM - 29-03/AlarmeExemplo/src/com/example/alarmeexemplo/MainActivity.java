package com.example.alarmeexemplo;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends Activity {

	private static final String CATEGORIA = "aula";
	private static final int SEGUNDOS = 5;
	private static final int REPETIR = 10;

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView text = new TextView(this); 
        text.setText("Alarme agendado para daqui a " + SEGUNDOS + " segundos.\nE repetir a cada 10.");
        setContentView(text);
        
        // Agendar o Alarme em 5 SEgundos
        
        agendar(SEGUNDOS);
    }

    private void agendar(int segundos) {
		Intent it = new Intent("EXECUTAR_ALARME");
		PendingIntent p = PendingIntent.getBroadcast(getApplicationContext(), 0, it, 0);
		
		// Para executar o alarme depois de x segundos a partir de agora
		
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(System.currentTimeMillis());
		c.add(Calendar.SECOND, segundos);
		
		// Agendar o alarme
		
		AlarmManager alarme = (AlarmManager)getSystemService(ALARM_SERVICE);
		long time = c.getTimeInMillis();
		
		// Repetir a cada 10 segudnos
		
		alarme.setRepeating(AlarmManager.RTC_WAKEUP, time ,REPETIR,p);
		Log.i(CATEGORIA, "Alarme agendado para daqui a " + segundos + " segundos. Repetir a cada " + REPETIR);
	}

    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	Log.i(CATEGORIA,"onDestroy() - alarme cancelado.");
    	Intent it = new Intent("EXECUTAR_ALARME");
    	PendingIntent p = PendingIntent.getBroadcast(getApplicationContext(), 0, it, 0);
    	
    	// Cancelar o alarme
    	AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am .cancel(p);
    }
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
