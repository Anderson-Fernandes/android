package com.example.alarmeexemplo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class ReceberAlarme extends BroadcastReceiver {

	private static final String CATEGORIA = "aula";	
	
	@Override
	public void onReceive(Context context, Intent intent) {
		String alarmeTexto = "Alarme Disparado!";
		Log.i(CATEGORIA, alarmeTexto);
		Toast.makeText(context,alarmeTexto, Toast.LENGTH_SHORT).show();
	}

}
