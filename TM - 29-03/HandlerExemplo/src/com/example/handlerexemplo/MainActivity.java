package com.example.handlerexemplo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {

    protected static final int Mensagem_teste = 1;
    private Handler handler = new TesteHandler();
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btEnviar = (Button)findViewById(R.id.btEnviar);
        btEnviar.setText("Atualizar o texto em 5 segundos");
        btEnviar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Criar delay de 5 seg
				Message msg = new Message();
				msg.what = Mensagem_teste;
				//Envia a mensagem
				handler.sendMessageDelayed(msg, 5000);
				
			}
		});
        
        
    }
	
	//Handler utilizado para receber a mensagem
	private class TesteHandler extends Handler{
		@Override
		public void handleMessage(Message msg){
			//O atributo msg.what permite identificar a mensagem
			switch (msg.what){
			case Mensagem_teste:
				Toast.makeText(getApplicationContext(), "A mensagem chegou!", Toast.LENGTH_SHORT).show();
				break;
			} 
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
