package com.example.hello_world;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		setContentView(R.layout.second);
		
		Bundle extras = getIntent().getExtras();
		String login = extras.getString("login");
		
		TextView txt_nome = (TextView) findViewById(R.id.txt_nome);
		txt_nome.setText("Bem vindo: "+ login);
	}
}
